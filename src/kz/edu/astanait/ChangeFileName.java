package kz.edu.astanait;

import java.io.File;
import java.util.Optional;

public class ChangeFileName implements Runnable {
    private final int id;
    File file;

    public ChangeFileName(int id, File file) {
        this.id = id;
        this.file = file;
    }

    @Override
    public void run() {
        System.out.println("Thread rename file with id:"+ id);
        File file1 = new File("files/"+ id +"."+ optionalStringHandler(file.getName()).get());
        file.renameTo(file1);
        try {
            Thread.sleep(300);
        } catch (InterruptedException exception){
            exception.printStackTrace();
        }
    }

    public Optional<String> optionalStringHandler(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
