package kz.edu.astanait;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        File fname = new File("files");
        List<File> files = Arrays.asList(fname.listFiles().clone());
        int size = Runtime.getRuntime().availableProcessors();
        Thread[] threads = new Thread[size];
        int temp = size-1;
        for(int i=0 ; i<files.size(); i++) {
            threads[temp--] = new Thread(new ChangeFileName(i, files.get(i)));
            threads[temp + 1].start();
            if (temp == -1) {
                temp = size - 1;
            }
        }

    }

}
